let [...dragItems] = document.querySelectorAll('.draggable');
let [...radioItems] = document.querySelectorAll('.radioIn');
let result = document.querySelector('.price > p');
let sauces = document.querySelector('.sauces > p');
let topings = document.querySelector('.topings > p');
let dropZone = document.querySelector('.table');
let sizePrice = 0;
let totalPrice = 0;

console.log([...dragItems], [...radioItems]);

dragItems.forEach((dragItem) => {
    dragItem.addEventListener('dragstart', handlerDragstart);
});

function handlerDragstart () {
    this.setAttribute('id', 'active');
};

dropZone.addEventListener('dragover', handlerDragover);
dropZone.addEventListener('dragenter', handlerDragenter);
dropZone.addEventListener('dragleave', handlerDragleave);
dropZone.addEventListener('drop', handlerDrop);

function handlerDragover(event) {
    event.preventDefault();
    //console.log('dragover');
};

function handlerDragenter() {
    dropZone.classList.add('hovered');
    //console.log('dragenter', this);
};

function handlerDragleave() {
    dropZone.classList.remove('hovered');
    //console.log('dragleave', this);
};

function handlerDrop() {
    let ingr = document.querySelector('#active');
    ingr.removeAttribute('id');
    let ingrClone = ingr.cloneNode();
    dropZone.append(ingrClone);
    dropZone.classList.remove('hovered');
    let priceOfIngr = parseInt(ingr.getAttribute('data-price'));
    totalPrice += priceOfIngr;
    result.innerHTML = `ЦІНА: ${totalPrice}`
    if (ingr.getAttribute('data-name') === 'sauce') {
        sauces.innerHTML += `${ingr.getAttribute('data-value')},`
    } else if (ingr.getAttribute('data-name') === 'toping') {
        topings.innerHTML += `${ingr.getAttribute('data-value')},`
    }
};

radioItems.forEach((item) => {
    item.addEventListener('change', () => {
        
        if (item.value == "small") {
            sizePrice = 50;
        } else if (item.value == "mid") {
            sizePrice = 75;
        } else {
            sizePrice = 100;
        }
        totalPrice += sizePrice;
        result.innerHTML = `ЦІНА: ${totalPrice}`
    })
})

let bannerBtn = document.querySelector('#banner > button');

bannerBtn.addEventListener('mouseover', (event) => {
    let banner = document.getElementById('banner');

    banner.style.right = Math.random() * (document.documentElement.clientWidth - event.target.offsetWidth) + 'px';
    banner.style.bottom = Math.random() * (document.documentElement.clientHeight - event.target.offsetHeight) + 'px';
});

let submitBtn = document.querySelector('input[name ="btnSubmit"]');

submitBtn.addEventListener('click', () => {
    let name = document.querySelector('input[name="name"]');
    let phone = document.querySelector('input[name="phone"]');
    let email = document.querySelector('input[name="email"]');
    
    if (name.value.search(/[a-zA-Z0-9]/) === -1 || 
        phone.value.search(/\+380\d{9}/) === -1 || 
        email.value.search(/\b[a-z0-9._]+@[a-z0-9._]+\.[a-z]{2,4}\b/)) {
            alert("Неверно введены данные!");
        } else {
            window.location.href = "./thank-you.html";
            submitBtn.className = "valid";
    }
});

function validateForm(ev) {
    let invalid = false;

    for (let i = 0; i = this.elements.length; i++) {
        let e = this.elements[i];
        if (e.type == "text" || e.type == "email" || e.type == "phone" && e.onchange !=null) {
            e.onchange();
            if (e.className == "error") {
                invalid = true;
            }
        }
    }

    if (invalid) {
        alert("Error");
        e.preventDefault();
    }
}


/*function randomInt(min, max) {
    return Math.random() * (max - min) + min;
};*/

